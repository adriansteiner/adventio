#ifdef DAY_2

#include "qpn/qpn.h"     // QP-nano framework
#include "Arduino.h"
#include "AIO.h"

typedef struct Blinky {
/* protected: */
    QActive super;
} Blinky;

// various constants for the application...
enum {
    BSP_TICKS_PER_SEC = 100,
    LED_ON_BOARD = 13,               // the pin number of the on-board LED (L)
    LED_ON_BREAD_BOARD = 2           // the pin number where we connected the LED
};

// AO instances and event queue buffers for them...
Blinky AO_Blinky;
static QEvt l_blinkyQSto[10]; // Event queue storage for Blinky

QActiveCB const Q_ROM QF_active[] = {
    { (QActive *)0,           (QEvt *)0,        0U                  },
    { (QActive *)&AO_Blinky,  l_blinkyQSto,     Q_DIM(l_blinkyQSto) }
};

// define the states
static QState Blinky_initial(Blinky * const me);
static QState Blinky_onBoard_LED_ON(Blinky * const me);
static QState Blinky_other_LED_ON(Blinky * const me);


// implement the AIO methods to initialize the board
void AIO_init_qpn(void)
{
    // initialize the QF-nano framework
    QF_init(Q_DIM(QF_active));

    // initialize all AOs...
    QActive_ctor(&AO_Blinky.super, Q_STATE_CAST(&Blinky_initial));
}

void AIO_init_hardware(void)
{
    pinMode(LED_ON_BOARD, OUTPUT);
    pinMode(LED_ON_BREAD_BOARD, OUTPUT);
}

// the actual state machine
static QState Blinky_initial(Blinky * const me) {
    /*${AOs::Blinky::SM::initial} */
        QActive_armX((QActive *)me, 0U,
                     BSP_TICKS_PER_SEC/2U, BSP_TICKS_PER_SEC/2U);
    return Q_TRAN(&Blinky_onBoard_LED_ON);
}
/*${AOs::Blinky::SM::off} ..................................................*/
static QState Blinky_onBoard_LED_ON(Blinky * const me) {
    QState status_;
    switch (Q_SIG(me)) {
        /*${AOs::Blinky::SM::off} */
        case Q_ENTRY_SIG: {
            digitalWrite(LED_ON_BOARD, HIGH);
            digitalWrite(LED_ON_BREAD_BOARD, LOW);
            status_ = Q_HANDLED();
            break;
        }
        /*${AOs::Blinky::SM::off::Q_TIMEOUT} */
        case Q_TIMEOUT_SIG: {
            status_ = Q_TRAN(&Blinky_other_LED_ON);
            break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}
/*${AOs::Blinky::SM::on} ...................................................*/
static QState Blinky_other_LED_ON(Blinky * const me) {
    QState status_;
    switch (Q_SIG(me)) {
        /*${AOs::Blinky::SM::on} */
        case Q_ENTRY_SIG: {
            digitalWrite(LED_ON_BOARD, LOW);
            digitalWrite(LED_ON_BREAD_BOARD, HIGH);
            status_ = Q_HANDLED();
            break;
        }
        /*${AOs::Blinky::SM::on::Q_TIMEOUT} */
        case Q_TIMEOUT_SIG: {
            status_ = Q_TRAN(&Blinky_onBoard_LED_ON);
            break;
        }
        default: {
            status_ = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status_;
}

#endif 
