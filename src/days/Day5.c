#ifdef DAY_5

#include "sm/blinky.h"
#include "Arduino.h"
#include "AIO.h"

// various constants for the application...
enum {
    BSP_TICKS_PER_SEC = 100,
    LED_ONE = 9,               // the pin number of the first led
    LED_TWO = 10,           // the pin number for the second led
    BUTTON_PIN = 2
};

// AO instances and event queue buffers for them...
Blinky AO_Blinky;
static QEvt l_blinkyQSto[10]; // Event queue storage for Blinky

QActiveCB const Q_ROM QF_active[] = {
    { (QActive *)0,           (QEvt *)0,        0U                  },
    { (QActive *)&AO_Blinky,  l_blinkyQSto,     Q_DIM(l_blinkyQSto) }
};

// blink duration in ticks
// 100 ticks are 1 second
uint8_t const blinkInterval = BSP_TICKS_PER_SEC/3; 

/* -------------------- Interrupt Handler -------------------------------*/
void buttonPressed(void) {
    QActive_postISR((QActive *)&AO_Blinky, BUTTON_PRESSED_SIG, 0);
}

/* -------------------- Interrupt Handler -------------------------------*/

// implement the AIO methods to initialize the board
void AIO_init_qpn(void)
{
    // initialize the QF-nano framework
    QF_init(Q_DIM(QF_active));

    // initialize all AOs...
    Blinky_ctor();
}

void AIO_init_hardware(void)
{
    pinMode(LED_ONE, OUTPUT);
    pinMode(LED_TWO, OUTPUT);

    pinMode(BUTTON_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonPressed, FALLING );
}

/* -------------------- BlinkyController implementation -------------------------------*/

/* -------------------- BlinkyController implementation -------------------------------*/



#endif 
