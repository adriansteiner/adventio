#ifndef _BLINKY_CONTROLLER_H_
#define _BLINKY_CONTROLLER_H_
#else

void BlinkyController_init(void);

void BlinkyController_on(void);

void BlinkyController_off(void);

#endif // _BLINKY_CONTROLLER_H_