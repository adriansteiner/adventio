#ifndef _AIO_H_
#define _AIO_H_
#else

void AIO_init_active_objects(void);

void AIO_init_hardware(void);

#endif /* _AIO_H_ */